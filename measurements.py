import logging
import json
import requests
import numpy as np
from collections import Counter


logger = logging.getLogger(__name__)


def compute_measurements(df_context, K, recommender_type, is_desktop):
    '''
    Returns recall@K, nDCG@K and diversity
    '''
    total_recall_at_k = 0
    total_nDCG = 0
    total = df_context.shape[0]
    recommendations_for_all_users = Counter({})
    random_answers_per_batch = 0

    if recommender_type == "oracle" or recommender_type == "popularity":
        if is_desktop:
            dummy_user_id = "dummy_user_id"
        else:
            dummy_user_id = "dummy_user_id".encode()
        precalc_recs_json = _get_memory_based_recommender_json(dummy_user_id, K, is_desktop, recommender_type="popularity")
        precalc_recs_list = _convert_recommendations_json_to_list(precalc_recs_json)

    for count, (user_id, watched_program_ids) in enumerate(df_context.iteritems(), 1):
        watched_program_ids = list(set(watched_program_ids)) # remove duplicates

        if recommender_type == "areena-recommender":
            recommendations_json = _get_areena_recommender_json(user_id, K, is_desktop)
        elif recommender_type == "areena-recommender-full-data":
            # This whitelists now childrens' programs from full data.
            recommendations_json = _get_areena_recommender_json(user_id, K, is_desktop, whitelist_category = "5-195")
        elif recommender_type == "memory-based":
            recommendations_json = _get_memory_based_recommender_json(user_id, K, is_desktop, recommender_type)
        elif recommender_type == "oracle":
            recommendations_json = _get_oracle_recommender_json(watched_program_ids, K, precalc_recs_list)
        elif recommender_type == "random":
            recommendations_json = _get_memory_based_recommender_json(user_id, K, is_desktop, recommender_type)
        elif recommender_type == "popularity":
            recommendations_json = precalc_recs_json

        # Only memory-based recommender might give random answers
        if _is_random_answer(recommendations_json):
            random_answers_per_batch += 1

        recommendations = _convert_recommendations_json_to_list(recommendations_json)

        watched_recs = list(set(watched_program_ids).intersection(recommendations[:K]))
        numerator = len(watched_recs)
        denominator = min(len(watched_program_ids), K)
        recall_at_k = numerator / denominator
        total_recall_at_k += recall_at_k

        total_nDCG += _nDCG(recommendations, watched_program_ids, K)

        # Gathers a Counter object (like a dict) from all recommended programs
        recommendations_for_all_users += Counter(recommendations)

        if count % 1000 == 0:
            logger.info("Done %s out of %s user ids (%s random answers).", count, total, random_answers_per_batch)
            random_answers_per_batch = 0

    diversity = _diversity(recommendations_for_all_users)

    return total_recall_at_k / total, total_nDCG / total, diversity


def _diversity(recommended_programs):
    '''
    Calculates diversity (Inverse Simpson index) of the recommended programs.
    More distinct program IDs equals more diversity.
    '''
    numerator = 0
    total_of_prog_ids = sum(recommended_programs.values())
    denominator = total_of_prog_ids * (total_of_prog_ids - 1)
    for program_id, amount in recommended_programs.items():
        numerator += (amount * (amount - 1))
    return 1 / (numerator / denominator)


def _nDCG(recommendations_list, watched_programs_list, K):
    """
    This function is inspired by another measurement function used at Yle.

    Compute NDCG@k given relevances of all items the recommendations.
    There are n items. Each item is an integer from 0 to n-1.
    :param recommendations_list: A list with items from most recommended to least. At most k are used.
    :param watched_programs_list: A list of programs the user has watched.
    :param K: amount of programs to use when calculating the nDCG value.
    """
    def build_relevance(recs, watched):
        '''
        Builds Numpy array of relevances of the recommendations.
        :param recs: a list of recommended program ids
        :param watched: a list of watched progrma ids
        :return: a numpy array length of recs denoting 0 or 1 if item at position i at recs is watched or not.
        '''
        relevance = []
        for prog in recs:
            if prog in watched:
                relevance.append(1)
            else:
                relevance.append(0)
        return np.asarray(relevance)

    relevances = build_relevance(recommendations_list, watched_programs_list)
    # If user hasn't watched any of the recommendations, then nDCG is 0.
    # Without this if clause normalizer would be inf in such situation.
    if (1 not in relevances):
        return 0
    recommendations = np.asarray(recommendations_list)
    recommendations = recommendations[:K]  # make sure there's at most k recommendations
    rank_weights = 1 / np.log2(np.arange(K) + 2)
    normalizer = 1 / (relevances[np.argsort(relevances)[::-1][:K]] @ rank_weights)
    return normalizer * (relevances @ rank_weights[:len(recommendations)])


def _convert_recommendations_json_to_list(json_data):
    recs = json_data['data']
    recs_list = [rec['id'] for rec in recs]
    return recs_list


def _is_random_answer(json_data):
    if 'random' in json_data:
        return json_data['random']
    else:
        return False


def _get_areena_recommender_json(user_id, K, is_desktop, whitelist_category = None):
    base_url = "" # Areena recommender's API is not public. Url deleted from this public repository.
    if is_desktop:
        user = "&desktop_id=" + user_id
    else:
        user = "&mobile_id=" + user_id.decode()
    full_url = base_url + user + "&limit=" + str(K)
    if whitelist_category:
        full_url += "&whitelist=" + whitelist_category
    try:
        data = json.loads(requests.get(full_url).text)
    except json.decoder.JSONDecodeError:
        logger.exception("Request url: %s", full_url)
    return data


def _get_memory_based_recommender_json(user_id, K, is_desktop, recommender_type ="memory-based"):
    assert recommender_type in ("memory-based", "random", "popularity")
    url_host = "http://localhost:5000/"
    if is_desktop:
        user_type = "desktop"
    else:
        user_id = user_id.decode()
        user_type = "mobile"
    url = url_host + "?user_id=" + user_id + "&limit=" + str(K) + "&user_type=" + user_type + "&recommender_type=" + recommender_type
    try:
        data = json.loads(requests.get(url).text)
    except json.decoder.JSONDecodeError:
        logger.exception("Request url: %s", url)
    return data


def _get_oracle_recommender_json(watched_program_ids, K, popularity_list):
    recommendations = watched_program_ids + popularity_list
    recommendations = _unique(recommendations)
    recommendations = recommendations[:K]
    recommendations = [{'id': item_id} for item_id in recommendations]
    return json.loads(json.dumps({'data': recommendations}, sort_keys=True, indent=4, separators=(',', ': ')))


def _unique(list):
    seen = set()
    return [x for x in list if not (x in seen or seen.add(x))]
