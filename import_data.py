import dask.dataframe as dd
import pandas as pd
import numpy as np
import re
import logging
import pytz
import udatetime


logger = logging.getLogger(__name__)


def import_data(play_data_path, metadata_path):
    df = _load_raw_data(play_data_path)
    metadata_df = _read_metadata(metadata_path)
    df = _clean_data(df, metadata_df)
    return df


def _load_raw_data(path):
    '''
    This function is inspired by similar function in Areena recommender.
    '''
    string_type = 'a64'
    header = ['yle_selva', 'c12', 'program_id', 'ns_st_pt', 'avg_yle_truelength', 'min_collectorreceived']
    dtypes = {'c12': string_type, 'ns_st_pt': np.float32, 'avg_yle_truelength': np.float32}

    def remove_special_chars(x):
        return re.sub(r'[^\x00-\x7F]+', '', x)

    logger.info("Loading playing data from %s.", path)
    df = dd.read_csv(path,
                     names=header, sep='|',
                     converters={'yle_selva': remove_special_chars, 'program_id': remove_special_chars},
                     dtype=dtypes,
                     encoding='utf-8',
                     blocksize=None)
    logger.info("Playing data loaded from %s.", path)
    return df


def _read_metadata(path):
    """
    This function is inspired by similar function in Areena recommender.
    """
    def parse_date(date):
        if isinstance(date, float) and np.isnan(date):
            return np.nan
        else:
            try:
                return udatetime.from_string(date).astimezone(pytz.UTC).replace(tzinfo=None)
            except ValueError:
                return np.nan

    header_names = ['item_id', 'series_id', 'type', 'typeMedia', 'categories', 'availability',
                    'start_time', 'end_time', 'title', 'image_id', 'episode', 'is_aggregatable',
                    'duration', 'publishers', 'audio_languages', 'subtitle_languages',
                    'title_languages', 'description_languages']
    dtypes = {'is_aggregatable': str, 'duration': float}
    logger.info("Loading metadata from %s.", path)
    df = pd.read_csv(path, sep='\t', header=None, names=header_names, dtype=dtypes,
                     parse_dates=['start_time', 'end_time'],
                     date_parser=parse_date)
    logger.info("Metadata loaded from %s.", path)
    return df


def _clean_data(df, metadata_df):
    logger.info("Cleaning up data started.")

    df = _reduce_to_childrens_programs(df, metadata_df)
    df = _reduce_to_videos(df, metadata_df)
    df = _add_series_id(df, metadata_df)

    # Clean empty strings and rows without necessary data values.
    df = df.map_partitions(lambda x: x.replace('', np.nan))
    df = df.dropna(subset=['ns_st_pt', 'avg_yle_truelength', 'program_id'])

    # combine IDs to one column and add another column for user type (desktop = 0 or mobile = 1)
    df = df.assign(user_id = df.yle_selva.combine_first(df.c12), is_desktop = df.yle_selva.notnull())

    # Calculate percentage played (this has to be done before compute()).
    df = df.assign(percentage_played = (df['ns_st_pt'] / df['avg_yle_truelength']))

    # Replace over 100 % played values with 100 % as individual play values shouldn't be over 100 %.
    df.percentage_played = df.percentage_played.mask(df['percentage_played'] > 1, 1)

    # Drop unnecessary columns.
    df = df.drop(['yle_selva', 'c12'], axis='columns')
    df = df.map_partitions(lambda x: x.replace(b'', np.nan))
    df = df.dropna(subset = ['user_id'])
    df = df.drop_duplicates(subset = ['user_id', 'program_id'])
    df = df.loc[df.percentage_played > 0.4]

    logger.info("Computing of dask dataframe started.")
    df = df.compute()
    logger.info("Computing of dask dataframe finished.")

    logger.info("Cleaning up data finished.")
    logger.debug("Dask dataframe shape: %s.", df.shape)
    return df


def _reduce_to_childrens_programs(df, metadata_df):
    is_children_program = metadata_df.set_index('item_id')['categories'].str.contains("5-195", na=0)
    correct_set = set(x for x in metadata_df.item_id if is_children_program[x])
    return df[df.program_id.isin(correct_set)]


def _reduce_to_videos(df, metadata_df):
    content_type = metadata_df.set_index('item_id')['typeMedia']
    correct_set = set(x for x in metadata_df.item_id if content_type[x] == 'TVContent')
    return df[df.program_id.isin(correct_set)]


def reduce_to_training_users(df, train_data_path, train_metadata_path):
    logger.debug("Dask dataframe shape before reducing to training users: %s.", df.shape)
    df_train = import_data(train_data_path, train_metadata_path)
    unique_train_users = df_train.user_id.unique()
    df = df[df.user_id.isin(unique_train_users)]
    logger.debug("Dask dataframe shape after reducing to training users: %s.", df.shape)
    return df


def _add_series_id(df, metadata_df):
    '''
    This function is originated from Areena recommender.
    '''
    item_to_series = _item_to_series(metadata_df)
    return df.assign(series_mapped_id = df.program_id.map(lambda pid: item_to_series.get(pid) or pid))


def _item_to_series(metadata_df):
    """
    This function is originated from Areena recommender.

    Convert program metadata into a mapping from single episodes to series.
    Note that this implicitly assumes the metadata contains program info for at least data_period_days days (as defined
    in the config object).
    :param metadata_df: a Dask dataframe of metadata, as returned by read_metadata
    :return: a dict with the program id -> series id mapping containing only program ids that should be hard
      aggregated together. Hard aggregation is always done, and is implemented in the backend (``support/``). Soft
      aggregation is done only sometimes, and is implemented in the frontend (``api/``).
    """
    def filter_program_ids_by_aggregatable_series(df):
        aggregatable_series_ids = df[df.is_aggregatable.astype(str) == 'True'].item_id.values
        return df[df.series_id.isin(aggregatable_series_ids)]

    def filter_unaggregatable_program_ids(df):
        "Filter out program ids that should not be hard aggregated even if they are part of an aggregatable series."
        return df[~df['type'].isin(['TVClip', 'RadioClip'])]

    def convert_to_series_id_by_item_id_dict(df):
        return df.set_index('item_id').series_id.to_dict()

    item_to_series = (metadata_df.pipe(filter_program_ids_by_aggregatable_series)
                                 .pipe(filter_unaggregatable_program_ids)
                                 .pipe(convert_to_series_id_by_item_id_dict))
    return item_to_series
