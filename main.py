import logging
import import_data as imp
import measurements
import sys
import argparse


logger = logging.getLogger()


def _init_logging(logger, log_to_stdout = True):
    LEVEL = logging.DEBUG
    LOG_FORMAT = "%(asctime)s - %(levelname)s - %(name)s - %(message)s"

    log_filepath = 'rec_eval.log'
    file_handler = logging.FileHandler(log_filepath)
    formatter = logging.Formatter(LOG_FORMAT)
    file_handler.setFormatter(formatter)
    file_handler.setLevel(LEVEL)
    logger.addHandler(file_handler)
    logger.setLevel(logging.DEBUG)
    logging.getLogger('requests').setLevel(logging.INFO)
    logging.getLogger('urllib3').setLevel(logging.INFO)
    logging.getLogger('chardet').setLevel(logging.INFO)

    if log_to_stdout:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(LEVEL)
        stdout_handler.setFormatter(formatter)
        logger.addHandler(stdout_handler)


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-data-path')
    parser.add_argument('--train-metadata-path')
    parser.add_argument('--test-data-path')
    parser.add_argument('--test-metadata-path')
    parser.add_argument('--K')
    parser.add_argument('--recommender-type',
                        choices=['areena-recommender', 'areena-recommender-full-data', 'oracle',
                                 'memory-based', 'random', 'popularity'],
                        default='areena-recommender')
    return parser.parse_args()


def main():
    _init_logging(logger)

    args = _parse_args()

    df = imp.import_data(args.test_data_path, args.test_metadata_path)

    # Comment this out if needed
    df = imp.reduce_to_training_users(df, args.train_data_path, args.train_metadata_path)

    logger.info("Starting to separate mobile and desktop dataframes.")
    df_desktop = df.loc[df.is_desktop == True]
    df_mobile = df.loc[df.is_desktop == False]
    logger.info("Finished separating mobile and desktop dataframes.")

    logger.info("Starting to group watched programs by desktop user.")
    df_desktop = df_desktop.groupby('user_id')['series_mapped_id'].apply(list)
    logger.info("Finished grouping watched programs by destkop user.")

    logger.info("Starting to group watched programs by mobile user.")
    df_mobile = df_mobile.groupby('user_id')['series_mapped_id'].apply(list)
    logger.info("Finished grouping watched programs by mobile user.")

    K = int(args.K)
    logger.info("K is set to %s.", K)
    logger.info("Recommender type set to %s.", args.recommender_type)

    logger.info("Starting to calculate measurements for desktop users.")
    recall_at_k_desktop, nDCG_at_k_desktop, diversity_desktop = measurements.compute_measurements(df_desktop, K,
                                                                                                  args.recommender_type,
                                                                                                  is_desktop = True)
    logger.info("Finished calculating results for desktop users. Recall@K: %s, nDCG@K: %s and Diversity: %s.",
                recall_at_k_desktop, nDCG_at_k_desktop, diversity_desktop)

    logger.info("Starting to calculate measurements for mobile users.")
    recall_at_k_mobile, nDCG_at_k_mobile, diversity_mobile = measurements.compute_measurements(df_mobile, K,
                                                                                               args.recommender_type,
                                                                                               is_desktop = False)
    logger.info("Finished calculating results for mobile users. Recall@K: %s, nDCGK: %s and Diversity: %s.",
                recall_at_k_mobile, nDCG_at_k_mobile, diversity_mobile)

    recall_at_k = (recall_at_k_desktop + recall_at_k_mobile) / 2
    nDCG_at_k = (nDCG_at_k_desktop + nDCG_at_k_mobile) / 2
    diversity = (diversity_desktop + diversity_mobile) / 2
    logger.info("Total results for all users: Recall@K: %s, nDCG@K: %s and diversity: %s.", recall_at_k, nDCG_at_k, diversity)


if __name__ == "__main__":
    main()
