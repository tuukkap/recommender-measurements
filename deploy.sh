#!/bin/bash

# Check current dir
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Edit these to your liking
INSTALL_DIR=/some/dir
SERVER_NAME=some-server
USER_NAME=some-user

# Create requirements.txt
source $DIR/venv/bin/activate
pip freeze > $DIR/requirements.txt
deactivate

echo "Moving files to server..."

rsync -z -r $DIR/*.py $DIR/requirements.txt $USER_NAME@$SERVER_NAME:$INSTALL_DIR

echo "Upgrading Python module requirements if necessary..."

ssh $SERVER_NAME "source $INSTALL_DIR/venv/bin/activate && pip install -U -r $INSTALL_DIR/requirements.txt && deactivate"

echo "Done!"
