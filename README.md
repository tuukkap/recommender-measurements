# Measurement scripts for Master's Thesis about recommendation engines

This repository contains measurement scripts for Master's Thesis about
recommendation engines for "Yle Lasten Areena".

Unfortunately the source data can't be shared publicly as it is real users' real data.
The purpose of this repository is to give an idea about the measurement methods used.

Calculate Recall, normalized DCG and Diversity for recommender engines.

## Requirements

* Python 3
* Modules listed in requirements.txt

## Usage

Formula used:

Recall@K = (# of items both recommended and watched during test period) /
           (# of played items during test period)
           
Normalized DCG: Please see [Wikipedia](https://en.wikipedia.org/wiki/Discounted_cumulative_gain#Normalized_DCG).

Diversity: Inverse Simpson index is used with the latter formula at Simpson index denoted with calligraphy l. Please see [Wikipedia](https://en.wikipedia.org/wiki/Diversity_index#Simpson_index).

This assumes to find watching data for testing period as csv files and
metadata as tsv in the local hard drive.

Command line arguments are:

* --train-data-path - relative path where to find the training data
* --train-metadata-path - relative path where to find training mmetadata
* --test-data-path - relative path where to find the test data
* --test-metadata-path - relative path where to find test metadata
* --K - the amount to be used with recall@K and NDCG
* --recommender-type - Choises are: areena-recommender, oracle and random. Default is areena-recommender. Oracle means perfect recommender (meant to be used for testing purposes).

Provide the paths and K as command line arguments when starting the script, e.g.:

    python3 main.py --train-data-path="train_data/*.csv" --train-metadata-path="train_data/metadata.tsv" --test-data-path="test_data/*.csv" --test-metadata-path="test_data/metadata.tsv" --K=20 --recommender-type="memory-based"
    
## License

See the LICENSE file at the root of this repository for license rights and limitations (EPL 1.0).
